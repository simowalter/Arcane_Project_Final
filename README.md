# README

## Auteurs:


* Walter Simo
* Aurelien Macron
* Karima Chliah
* Mouna Ridou

---

## Outils Techniques:
* ![](https://www.blackcreeper.com/wp-content/uploads/2020/04/kubernetes-logo-big.png)
* ![](https://miro.medium.com/max/400/1*ANDxSZMbvvhaxwqdI-6rPw.png)
* ![](https://cdn.thenewstack.io/media/2021/11/e8174f5e-rke2.png)
* ![](https://gitlab.adullact.net/uploads/-/system/project/avatar/637/gitlab-ci-cd-logo_2x.png)

* ![](https://img-0.journaldunet.com/5dyx1x7c8hipe3JS92vWnli_LIc=/1500x/smart/f628d3865a7f4ba4ab1f125beba18a58/ccmcms-jdn/19946149.jpg)
* ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Ansible_logo.svg/1200px-Ansible_logo.svg.png)
  

## Cloud Provider
* ![](https://a0.awsstatic.com/libra-css/images/logos/aws_logo_smile_1200x630.png)



---
## GOAL

### Build a Gitlab CI/CD pipeline that creates an infrastruce of 2 ec2 instances via Terraform, then install and configure RKE2 on the cluster via Ansible and Finally deploy a web-application on this cluster with Helm.

---

N.B: _The pipeline includes a final stage *destroy* whichi is triggered manually and which destroy all the infra created._