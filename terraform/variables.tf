variable "instance_name" {
    description = "Name of the instance to be created"
    type = string
    default = "ARCANE_EC2"
}

variable "region" {
    type = string
    default = "eu-west-3" # paris
}

variable "instance_type" {
    type = string
    default = "t3.large"
}

variable "ami_id" {
    description = "The AMI to use"
    type = string
    default = "ami-03c476a1ca8e3ebdc"  # ubuntu 22.04
}

variable "number_of_instances" {
    description = "Number of instances to be created"
    type = number
    default = 2
}

variable "key_name" {
    type = string
    default = "ARCANE_Key"
}

## ----------------------
variable "ingress_rules" {
  type = list(object({
    port     = number
    protocol = string
  }))

  default = [
    { port = 9345, protocol = "tcp" },
    { port = 6443, protocol = "tcp" },
    { port = 8472, protocol = "udp" },
    { port = 10250, protocol = "tcp" },
    { port = 2379, protocol = "tcp" },
    { port = 2380, protocol = "tcp" },
    { port = 8472, protocol = "udp" },
    { port = 4240, protocol = "tcp" },
    { port = 179, protocol = "tcp" },
    { port = 4789, protocol = "udp" },
    { port = 5473, protocol = "tcp" },
    { port = 9098, protocol = "tcp" },
    { port = 9099, protocol = "tcp" },
    { port = 8472, protocol = "udp" },
    { port = 51820, protocol = "udp" },
    { port = 51821, protocol = "udp" },
    { port = 8000, protocol = "tcp" },
    { port = 8001, protocol = "tcp" },
  ]
}

variable "sec_group_arcane" {
    type = string
    default = "ARCANE_security_group"
}
