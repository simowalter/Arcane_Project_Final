resource "tls_private_key" "arcane" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.arcane.public_key_openssh
}

resource "aws_security_group" "sec_group_arcane" {
  name        = "${var.sec_group_arcane}"
  description = "Arcane security group"

  # allow connection for rke2
dynamic "ingress" {
    for_each = var.ingress_rules
    content {
        from_port   = ingress.value.port
        to_port     = ingress.value.port
        protocol    = ingress.value.protocol
        cidr_blocks = ["0.0.0.0/0"]
      }
  }

  # allow specials connection for rke2 (icmp and range)
  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 30000
    to_port = 32767
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # allow ssh connection
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow http connection
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow https connection
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "VM_Arcane_Project" {
  ami = "${var.ami_id}" 
  count = "${var.number_of_instances}" 
  instance_type = "${var.instance_type}"
 
  tags = {
    Name = "${var.instance_name}"
  }

  ## nouvelle attributs
  
  key_name      = aws_key_pair.generated_key.key_name
  # security_groups = [aws_security_group.sec_group_arcane.name]
  security_groups = ["Arcane_sec_group"]
}

#save the private key
resource "local_file" "private_key_file" {
  content  = tls_private_key.arcane.private_key_pem
  filename = "${var.key_name}.pem"
}

# create inventory file
resource "local_file" "inventory_file" {
  content  = templatefile("inventory.tmpl", {
    public_ips = [for i in aws_instance.VM_Arcane_Project: i.public_ip]
  })
  filename = "inventory.ini"
}


resource "local_file" "ip_master" {
  content  = aws_instance.VM_Arcane_Project[0].public_ip
  filename = "IP_master_node.ini"
}


resource "local_file" "ip_worker" {
  content  = aws_instance.VM_Arcane_Project[1].public_ip
  filename = "IP_worker_node.ini"
}

output "inventory" {
  value = templatefile("inventory.tmpl", {
    public_ips = [for i in aws_instance.VM_Arcane_Project: i.public_ip]
  })
}
